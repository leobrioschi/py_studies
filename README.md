# py_studies

Some stuff in python to share. Each python notebook should have its' own explanation. Please visit my website https://leobrioschi.gitlab.io

## Menu
1. Python polynomial expression parser and solver: parser for LaTeX and python syntax, \Re solver
2. Alpha Vantage intraday downloader and parser.

## Authors and acknowledgment
PhD Candidate in Finance! More at  https://leobrioschi.gitlab.io

## License
No License

